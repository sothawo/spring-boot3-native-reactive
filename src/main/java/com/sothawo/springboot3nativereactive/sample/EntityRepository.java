/*
 * (c) Copyright 2023 sothawo
 */
package com.sothawo.springboot3nativereactive.sample;

import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;

/**
 * @author P.J. Meisch (pj.meisch@sothawo.com)
 */
public interface EntityRepository extends ReactiveElasticsearchRepository<Entity, String> {
}
