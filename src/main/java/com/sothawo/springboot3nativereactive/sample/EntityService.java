/*
 * (c) Copyright 2023 sothawo
 */
package com.sothawo.springboot3nativereactive.sample;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author P.J. Meisch (pj.meisch@sothawo.com)
 */
@Service
public class EntityService {

		private final EntityRepository repository;

		public EntityService(EntityRepository repository) {
				this.repository = repository;
		}

		public Mono<Entity> save(Entity entity) {
				return repository.save(entity);
		}

		public Flux<Entity> findAll() {
				return repository.findAll();
		}
}
