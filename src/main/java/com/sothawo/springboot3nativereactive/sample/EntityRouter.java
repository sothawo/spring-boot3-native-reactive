/*
 * (c) Copyright 2023 sothawo
 */
package com.sothawo.springboot3nativereactive.sample;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * @author P.J. Meisch (pj.meisch@sothawo.com)
 */
@Configuration("EntityRouterConfiguration")
public class EntityRouter {

		@Bean
		RouterFunction<ServerResponse> entityRouter(EntityHandler handler) {
				return nest(
								path("/sample-entity"),
								route(POST("/entity"), handler::save)
												.andRoute(GET("/"), handler::findAll)
				);
		}
}
