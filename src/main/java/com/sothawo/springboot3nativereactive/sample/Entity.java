package com.sothawo.springboot3nativereactive.sample;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.lang.Nullable;

/**
 * @author P.J. Meisch (pj.meisch@sothawo.com)
 */
@Document(indexName = "sample-entity")
public record Entity(@Id @Nullable String id,
										 @Nullable @Field(type = FieldType.Text) String name) {
}
