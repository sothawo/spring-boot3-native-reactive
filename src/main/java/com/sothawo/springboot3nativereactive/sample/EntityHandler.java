/*
 * (c) Copyright 2023 sothawo
 */
package com.sothawo.springboot3nativereactive.sample;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @author P.J. Meisch (pj.meisch@sothawo.com)
 */

@Component
public class EntityHandler {

		private final EntityService service;

		public EntityHandler(EntityService service) {
				this.service = service;
		}

		public Mono<ServerResponse> save(ServerRequest request) {
				return request.bodyToMono(Entity.class)
								.map(service::save)
								.flatMap(entity ->
												ServerResponse.ok().body(entity, Entity.class)
								);
		}

		public Mono<ServerResponse> findAll(ServerRequest request) {
				return ServerResponse.ok().body(service.findAll(), Entity.class);
		}
}
