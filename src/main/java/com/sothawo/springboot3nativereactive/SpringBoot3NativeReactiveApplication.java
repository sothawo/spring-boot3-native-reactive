package com.sothawo.springboot3nativereactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot3NativeReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot3NativeReactiveApplication.class, args);
	}

}
