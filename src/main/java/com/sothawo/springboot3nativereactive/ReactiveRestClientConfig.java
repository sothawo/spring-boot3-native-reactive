/*
 * (c) Copyright 2023 sothawo
 */
package com.sothawo.springboot3nativereactive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchClients;
import org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchConfiguration;
import org.springframework.data.elasticsearch.core.RefreshPolicy;
import org.springframework.data.elasticsearch.support.HttpHeaders;
import org.springframework.data.mapping.model.CamelCaseSplittingFieldNamingStrategy;
import org.springframework.data.mapping.model.FieldNamingStrategy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author P.J. Meisch (pj.meisch@sothawo.com)
 */
@Configuration
public class ReactiveRestClientConfig extends ReactiveElasticsearchConfiguration {

		private static final Logger LOGGER = LoggerFactory.getLogger(ReactiveRestClientConfig.class);

		public ClientConfiguration clientConfiguration() {
				return ClientConfiguration.builder() //
								.connectedTo("localhost:9200") //
//            .usingSsl()
//             .usingSsl(NotVerifyingSSLContext.getSslContext()) //
								.withProxy("localhost:8080")
//            .withPathPrefix("ela")
								.withBasicAuth("elastic", "hcraescitsale") //
								.build();
		}

		@Override
		protected RefreshPolicy refreshPolicy() {
				return RefreshPolicy.IMMEDIATE;
		}

		@Override
		protected FieldNamingStrategy fieldNamingStrategy() {
				return new KebabCaseFieldNamingStrategy();
		}

		static class KebabCaseFieldNamingStrategy extends CamelCaseSplittingFieldNamingStrategy {
				public KebabCaseFieldNamingStrategy() {
						super("-");
				}
		}
}
